# On demand gitlab-runner with sudo rights
This is a proof-of-concept of a on-demand gitlab-runner in AWS EC2. Many users, myself included, need to run gitlab-runner with sudo priviledge. The use case presented here to build docker images every time the Dockerfile is updated under deployment. To have a virtual machine running in standby is not cost-effective. This proof-of-concept design turns on an EC2 instance before the job and turns it off after it. It makes use of a shared gitlab-runner, AWS lambda functions and a free-tier AWS EC2 instance. 

![Illustration](./figs/serverless-gitlab-runner.png)


## Installation
### Prerequisite
You need to have
- An AWS EC2 instance (A free-tier EC2 suffices. Make sure you have created key pairs and you can SSH into the instance). Stop the instance when you are not using it.
- An AWS IAM role and its credential information, e.g. AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.

### Setup/installation
- Clone the repository and push to gitlab.com.
- Set up CI/CD environment variables with IAM credentials. Follow the guide [Authenticate Gitlab with AWS](https://docs.gitlab.com/ee/ci/cloud_deployment/#authenticate-gitlab-with-aws).
- Install gitlab-runner and docker in your EC2 instance (you need to SSH into the instance):
    - Install gitlab-runner:
    ```s
    $ curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
    $ sudo dpkg -i gitlab-runner_amd64.deb
    ```
    - Register gitlab-runner:
    ```s
    $ sudo gitlab-runner register \
        --url "https://gitlab.com/" \
        --registration-token "<your-registration-token>" \
        --executor "shell" \
        --shell "bash" \
        --description "docker image builder" \
        --tag-list "runner-vm" \
        --run-untagged="false"
    ```
    `<your-registration-token>` can be found following this [stackoverflow post](https://devops.stackexchange.com/questions/4615/where-to-obtain-the-token-to-register-a-gitlab-runner). 
    - Install Docker following the [official docker guide](https://docs.docker.com/engine/install/ubuntu/)
    - Add gitlab-runner to docker group.
    ```s
    $ sudo groupadd docker
    $ sudo usermod -aG docker gitlab-runner
    ```
- Set up lambda functions to start and stop your AWS EC2 instances. Follow the guide [here](https://www.howtoforge.com/aws-lambda-function-to-start-and-stop-ec2-instance/), but use the stop and stop scripts in the `src` folder. 
- Create function urls for the lambda function, following this guide [here](https://dev.to/aws-builders/how-to-create-lambda-function-urls-c88)


## Test the on-demand pipeline
Make some change to `deployment/Dockerfile`, e.g. add a dependency or a comment. Commit the changes. If you are editing locally, make sure to push to remote. This will trigger the pipeline and you can triger the job for building the docker image in the pipeline. 


## Contributing
Any improvement is welcome! 


## Project status
The proof-of-concept works with the public gitlab. To deploy it on a corporate gitlab, some kinks need to be ironed out. 

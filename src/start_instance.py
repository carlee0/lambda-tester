import json
import boto3
region = 'us-east-1'
instances = ['i-094a154fb3e1ed9aa']
ec2 = boto3.client('ec2', region_name=region)
ec2_resource = boto3.resource('ec2', region_name=region)
def lambda_handler(event, context):
    ec2.start_instances(InstanceIds=instances)

    for instance in instances:
        insta = ec2_resource.Instance(instance)
        insta.wait_until_running(
            Filters=[
                {
                    'Name': 'instance-state-name',
                    'Values': [
                        'running',
                    ]
                },
            ]
        )
    print('started your instances: ' + str(instances))
    return {
        'body': json.dumps('Started your instances' + str(instances))
    }
